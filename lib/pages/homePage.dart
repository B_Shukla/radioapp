import 'package:alan_voice/alan_voice.dart';
import 'package:audioplayers/audioplayers.dart';
// import 'package:audioplayers/audioplayers_web.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:radio_app/model/radio.dart';
import 'package:radio_app/utils/ai_utils.dart';
import 'package:velocity_x/velocity_x.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<CustomRadio> radios = [];
  CustomRadio selectedRadio;
  Color selectedColor;
  bool isPlaying = false;

  final sugg = [
    "play",
    "stop",
    "play rock music",
    "play 107 fm",
    "play next",
    "play 104 fm",
    "pause",
    "play previous",
    "play pop music"
  ];

  final AudioPlayer audioPlayer = AudioPlayer();

  @override
  void initState() {
    // ignore: todo
    // TODO: implement initState
    super.initState();
    setupAlan();
    fetchRadios();
    audioPlayer.onPlayerStateChanged.listen((event) {
      if (event == AudioPlayerState.PLAYING) {
        isPlaying = true;
      } else {
        isPlaying = false;
      }
      setState(() {});
    });
  }

  fetchRadios() async {
    final radioJson = await rootBundle.loadString('assets/radio.json');
    radios = RadioList.fromJson(radioJson).radios;
    selectedRadio = radios[0];
    selectedColor = Color(int.tryParse(selectedRadio.color));
    print(radios);
    setState(() {});
  }

  playMusic(String url) {
    audioPlayer.play(url);
    selectedRadio = radios.firstWhere((element) => element.url == url);
    print(selectedRadio);
    setState(() {});
  }

  setupAlan() {
    AlanVoice.addButton(
        "2688f63e32efb65617c0c9d6a7c86eda2e956eca572e1d8b807a3e2338fdd0dc/stage",
        buttonAlign: AlanVoice.BUTTON_ALIGN_RIGHT);
    AlanVoice.callbacks.add((command) => handleCommand(command.data));
  }

  handleCommand(Map<String, dynamic> response) {
    switch (response["command"]) {
      case "play":
        playMusic(selectedRadio.url);
        break;
      case "stop":
        audioPlayer.stop();
        break;
      case "next":
        final index = selectedRadio.id;
        CustomRadio newRadio;
        if (index == radios.length - 1) {
          newRadio = radios.firstWhere((element) => element.id == 1);
          radios.remove(newRadio);
          radios.insert(0, newRadio);
        } else {
          newRadio = radios.firstWhere((element) => element.id == index + 1);
          radios.remove(newRadio);
          radios.insert(0, newRadio);
        }
        playMusic(newRadio.url);
        break;
      case "prev":
        final index = selectedRadio.id;
        CustomRadio newRadio;
        if (index == 0) {
          newRadio = radios.firstWhere((element) => element.id == 1);
          radios.remove(newRadio);
          radios.insert(0, newRadio);
        } else {
          newRadio = radios.firstWhere((element) => element.id == index - 1);
          radios.remove(newRadio);
          radios.insert(0, newRadio);
        }
        playMusic(newRadio.url);
        break;
      case "play_channel":
        final id = response["id"];
        audioPlayer.pause();
        CustomRadio newRadio = radios.firstWhere((element) => element.id == id);
        radios.remove(newRadio);
        radios.insert(0, newRadio);
        playMusic(newRadio.url);
        break;
      default:
        print("Command was ${response["command"]}");
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: Container(
          color: selectedColor ?? AIColors.primaryColor,
          child: radios != null
              ? [
                  100.heightBox,
                  "All Channels".text.xl.white.semiBold.make().px16(),
                  20.heightBox,
                  ListView(
                          padding: Vx.m0,
                          shrinkWrap: true,
                          children: radios
                              .map((e) => ListTile(
                                    leading: CircleAvatar(
                                      backgroundImage: NetworkImage(e.icon),
                                    ),
                                    title: "${e.name} FM".text.white.make(),
                                    subtitle: e.tagline.text.white.make(),
                                  ))
                              .toList())
                      .expand()
                ].vStack(crossAlignment: CrossAxisAlignment.start)
              : const Offstage(),
        ),
      ),
      body: Stack(
        children: [
          VxAnimatedBox()
              .size(context.screenWidth, context.screenHeight)
              .withGradient(LinearGradient(colors: [
                AIColors.secondaryColor,
                selectedColor ?? AIColors.primaryColor
              ], begin: Alignment.topLeft, end: Alignment.bottomRight))
              .make(),
          [
            AppBar(
              title: "AI Radio".text.xl4.bold.white.make().shimmer(
                  primaryColor: Vx.purple200, secondaryColor: Vx.white),
              centerTitle: true,
              backgroundColor: Colors.transparent,
              elevation: 0.0,
            ).h(100.0).p16(),
            "Start with - Hey Alan".text.italic.semiBold.white.make(),
            10.heightBox,
            VxSwiper.builder(
              itemCount: sugg.length,
              height: 50.0,
              viewportFraction: 0.35,
              autoPlay: true,
              autoPlayAnimationDuration: 3.seconds,
              autoPlayCurve: Curves.linear,
              enableInfiniteScroll: true,
              itemBuilder: (context, index) {
                final s = sugg[index];
                return Chip(
                    label: s.text.make(), backgroundColor: Vx.randomColor);
              },
            )
          ].vStack(),
          radios != null
              ? VxSwiper.builder(
                  // ignore: null_aware_before_operator
                  // itemCount: radios?.length > 0 ? radios.length : 0,
                  itemCount: radios.length,
                  aspectRatio:1.0,
                  enlargeCenterPage: true,
                  onPageChanged: (index) {
                    final colorHex = radios[index].color;
                    selectedRadio = radios[index];
                    selectedColor = Color(int.tryParse(colorHex));
                    setState(() {});
                  },
                  itemBuilder: (context, index) {
                    final rad = radios[index];
                    return VxBox(
                            child: ZStack([
                      Positioned(
                          top: 0.0,
                          right: 0.0,
                          child: VxBox(
                                  child:
                                      rad.category.text.uppercase.white.make())
                              .height(40.0)
                              .black
                              .alignCenter
                              .withRounded(value: 10.0)
                              .make()),
                      Align(
                          alignment: Alignment.bottomCenter,
                          child: VStack([
                            rad.name.text.xl3.white.bold.make(),
                            5.heightBox,
                            rad.tagline.text.sm.white.semiBold.make()
                          ], crossAlignment: CrossAxisAlignment.center)),
                      Align(
                          alignment: Alignment.center,
                          child: [
                            Icon(CupertinoIcons.play_circle,
                                color: Colors.white),
                            10.heightBox,
                            "Double tap to play".text.gray300.make()
                          ].vStack())
                    ]))
                        .clip(Clip.antiAlias)
                        .bgImage(DecorationImage(
                            image: NetworkImage(rad.image),
                            fit: BoxFit.cover,
                            colorFilter: ColorFilter.mode(
                                Colors.black.withOpacity(0.3),
                                BlendMode.darken)))
                        .border(color: Colors.black, width: 5.0)
                        .withRounded(value: 60.0)
                        .make()
                        .onInkDoubleTap(() {
                      playMusic(rad.url);
                    }).p16();
                  },
                ).centered()
              : Center(
                  child:
                      CircularProgressIndicator(backgroundColor: Colors.white)),
          Align(
            alignment: Alignment.bottomCenter,
            child: [
              if (isPlaying)
                "Playing Now - ${selectedRadio.name} FM"
                    .text
                    .white
                    .makeCentered(),
              Icon(
                      isPlaying
                          ? CupertinoIcons.stop_circle
                          : CupertinoIcons.play_circle,
                      color: Colors.white,
                      size: 50.0)
                  .onInkTap(() {
                if (isPlaying) {
                  audioPlayer.stop();
                } else {
                  playMusic(selectedRadio.url);
                }
              })
            ].vStack(),
          ).pOnly(bottom: context.percentHeight * 12)
        ],
        fit: StackFit.expand,
        clipBehavior: Clip.antiAlias,
      ),
    );
  }
}

class MobileWindowSize {}
