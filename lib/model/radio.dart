import 'dart:convert';

import 'package:flutter/foundation.dart';

class RadioList {
  final List<CustomRadio> radios;
  RadioList({
     this.radios,
  });

  RadioList copyWith({
    List<CustomRadio> radios,
  }) {
    return RadioList(
      radios: radios ?? this.radios,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'radios': radios?.map((x) => x.toMap())?.toList(),
    };
  }

  factory RadioList.fromMap(Map<String, dynamic> map) {
    return RadioList(
      radios: List<CustomRadio>.from(map['radios']?.map((x) => CustomRadio.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory RadioList.fromJson(String source) =>
      RadioList.fromMap(json.decode(source));

  @override
  String toString() => 'RadioList(radios: $radios)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is RadioList && listEquals(other.radios, radios);
  }

  @override
  int get hashCode => radios.hashCode;
}

class CustomRadio {
  final int id;
  final String name;
  final String tagline;
  final String color;
  final String desc;
  final String url;
  final String icon;
  final String image;
  final String lang;
  final String category;
  final bool disliked;
  final int order;

  CustomRadio(
      this.id,
      this.name,
      this.tagline,
      this.color,
      this.desc,
      this.url,
      this.icon,
      this.image,
      this.lang,
      this.category,
      this.disliked,
      this.order);

  CustomRadio copyWith({
    int id,
    String name,
    String tagline,
    String color,
    String desc,
    String url,
    String icon,
    String image,
    String lang,
    String category,
    bool disliked,
    int order,
  }) {
    return CustomRadio(
      id ?? this.id,
      name ?? this.name,
      tagline ?? this.tagline,
      color ?? this.color,
      desc ?? this.desc,
      url ?? this.url,
      icon ?? this.icon,
      image ?? this.image,
      lang ?? this.lang,
      category ?? this.category,
      disliked ?? this.disliked,
      order ?? this.order,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'tagline': tagline,
      'color': color,
      'desc': desc,
      'url': url,
      'icon': icon,
      'image': image,
      'lang': lang,
      'category': category,
      'disliked': disliked,
      'order': order,
    };
  }

  factory CustomRadio.fromMap(Map<String, dynamic> map) {
    return CustomRadio(
      map['id'],
      map['name'],
      map['tagline'],
      map['color'],
      map['desc'],
      map['url'],
      map['icon'],
      map['image'],
      map['lang'],
      map['category'],
      map['disliked'],
      map['order'],
    );
  }

  String toJson() => json.encode(toMap());

  factory CustomRadio.fromJson(String source) => CustomRadio.fromMap(json.decode(source));

  @override
  String toString() {
    return 'CustomRadio(id: $id, name: $name, tagline: $tagline, color: $color, desc: $desc, url: $url, icon: $icon, image: $image, lang: $lang, category: $category, disliked: $disliked, order: $order)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is CustomRadio &&
        other.id == id &&
        other.name == name &&
        other.tagline == tagline &&
        other.color == color &&
        other.desc == desc &&
        other.url == url &&
        other.icon == icon &&
        other.image == image &&
        other.lang == lang &&
        other.category == category &&
        other.disliked == disliked &&
        other.order == order;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        name.hashCode ^
        tagline.hashCode ^
        color.hashCode ^
        desc.hashCode ^
        url.hashCode ^
        icon.hashCode ^
        image.hashCode ^
        lang.hashCode ^
        category.hashCode ^
        disliked.hashCode ^
        order.hashCode;
  }
}
