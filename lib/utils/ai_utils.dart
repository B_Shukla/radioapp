import 'package:flutter/cupertino.dart';
import 'package:velocity_x/velocity_x.dart';

mixin AIColors{
  static Color primaryColor = Vx.orange400;
  static Color secondaryColor = Vx.purple500;
}